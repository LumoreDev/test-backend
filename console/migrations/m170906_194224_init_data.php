<?php

use yii\db\Migration;

class m170906_194224_init_data extends Migration
{
    public function safeUp()
    {
        Yii::$app->db->createCommand()->batchInsert('{{%category}}', ['name'], [
            ['Фрукт'],
            ['Овощ'],
        ])->execute();
        Yii::$app->db->createCommand()->batchInsert('{{%type}}', ['category_id', 'name'], [
            [1, 'Яблоко'],
            [1, 'Банан'],
            [1, 'Груша'],
            [2, 'Помидор'],
            [2, 'Картофель'],
            [2, 'Огурец'],
        ])->execute();
        Yii::$app->db->createCommand()->batchInsert('{{%position}}', ['category_id', 'type_id', 'amount', 'provider', 'date', 'price'], [
            [1, 1, 5, 'Магнит', '09/6/2017', 100],
            [1, 2, 6, 'Ашан', '09/5/2017', 200],
            [1, 3, 7, 'Магнит', '09/6/2017', 100],
            [2, 4, 8, 'Ашан', '09/5/2017', 200],
            [2, 5, 9, 'Магнит', '09/6/2017', 100],
            [2, 6, 10, 'Ашан', '09/5/2017', 200],
        ])->execute();
    }

    public function safeDown()
    {
        echo "m170906_194224_init_data cannot be reverted.\n";

        return false;
    }
}
