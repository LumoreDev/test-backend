<?php

use yii\db\Migration;

class m170906_120229_table_category extends Migration
{
    public $tableName = '{{%category}}';
    public $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ], $this->tableOptions);

        $this->createIndex('idx-category-id', $this->tableName, 'id');
    }

    public function safeDown()
    {
        $this->dropIndex('idx-category-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
