<?php

use yii\db\Migration;

class m170906_125648_table_position extends Migration
{
    public $tableName = '{{%position}}';
    public $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'amount' => $this->integer()->notNull(),
            'provider' => $this->string()->notNull(),
            'date' => $this->string()->notNull(),
            'price' => $this->float()->notNull(),
        ], $this->tableOptions);

        // Foreign keys
        $this->addForeignKey('FK-position-category_id-category-id', $this->tableName, 'category_id', '{{%category}}', 'id', 'CASCADE');
        $this->addForeignKey('FK-position-type_id-type-id', $this->tableName, 'type_id', '{{%type}}', 'id', 'CASCADE');
        // Index
        $this->createIndex('idx-position-category_id', $this->tableName, 'category_id');
        $this->createIndex('idx-position-type_id', $this->tableName, 'type_id');
        $this->createIndex('idx-position-date', $this->tableName, 'date');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK-position-category_id-category-id', $this->tableName);
        $this->dropForeignKey('FK-position-type_id-type-id', $this->tableName);
        $this->dropIndex('idx-position-category_id', $this->tableName);
        $this->dropIndex('idx-position-type_id', $this->tableName);
        $this->dropIndex('idx-position-date', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
