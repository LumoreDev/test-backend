<?php

use yii\db\Migration;

class m170906_120430_table_type extends Migration
{
    public $tableName = '{{%type}}';
    public $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
        ], $this->tableOptions);

        // Foreign keys
        $this->addForeignKey('FK-type-category_id-category-id', $this->tableName, 'category_id', '{{%category}}', 'id', 'CASCADE');
        // Index
        $this->createIndex('idx-type-id', $this->tableName, 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK-type-category_id-category-id', $this->tableName);
        $this->dropIndex('idx-type-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
