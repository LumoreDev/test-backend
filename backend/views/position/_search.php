<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PositionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="position-search">

    <div class="row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="col-md-6">
            <?= $form->field($model, 'category_id')->dropDownList($categoryArray, ['id' => 'cat-id', 'prompt' => 'Все категории']) ?>

            <?= $form->field($model, 'type_id')->widget(DepDrop::classname(), [
                'options' => ['id' => 'subcat-id'],
                'pluginOptions' => [
                    'depends' => ['cat-id'],
                    'placeholder' => 'Все типы',
                    'url' => Url::to(['/position/types']),
                    'initialize' => true
                ]
            ]) ?>

            <?= $form->field($model, 'amount') ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'provider') ?>

            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'mm/dd/yyyy'
                ]
            ]) ?>
            <?= $form->field($model, 'price') ?>
        </div>


    </div>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
