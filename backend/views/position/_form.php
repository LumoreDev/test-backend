<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Position */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="position-form">

    <div class="row">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-6">

            <?= $form->field($model, 'category_id')->dropDownList($categoryArray, ['id' => 'cat-id']) ?>

            <?= $form->field($model, 'type_id')->widget(DepDrop::classname(), [
                'options' => ['id' => 'subcat-id'],
                'pluginOptions' => [
                    'depends' => ['cat-id'],
                    'placeholder' => false,
                    'url' => Url::to(['/position/types']),
                    'initialize' => true
                ]
            ]) ?>

            <?= $form->field($model, 'amount')->textInput() ?>

        </div>

        <div class="col-md-6">

            <?= $form->field($model, 'provider')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'mm/dd/yyyy'
                ]
            ]) ?>

            <?= $form->field($model, 'price')->textInput() ?>

        </div>

        <div class="col-md-12">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>


        <?php ActiveForm::end(); ?>

    </div>

</div>
