<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PositionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Positions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Position'), ['create'], ['class' => 'btn btn-success']) ?>
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Показать фильтр
        </button>
    </p>

    <div class="collapse" id="collapseExample">
        <div class="well">
            <?php  echo $this->render('_search', [
                'model' => $searchModel,
                'categoryArray' => $categoryArray,
                'typeArray' => $typeArray,
            ]); ?>
        </div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'type_id',
                'format' => 'text',
                'value' => function($model) {
                    return $model->type->name;
                }
            ],
            [
                'attribute' => 'category_id',
                'format' => 'text',
                'value' => function($model) {
                    return $model->category->name;
                }
            ],
            'amount',
            'provider',
            'date',
            'price',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
