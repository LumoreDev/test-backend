<?php

return [
    'Category' => 'Категория',
    'Type' => 'Тип',
    'Types' => 'Типы',
    'Create Type' => 'Добавить тип',
    'Amount' => 'Количество',
    'Provider' => 'Поставщик',
    'Date' => 'Дата поставки',
    'Price' => 'Цена',
    'Create Position' => 'Добавить позицию',
    'Update Position' => 'Редактировать позицию',
    'Positions' => 'Товары',
    'Categories' => 'Категории',
    'Create Category' => 'Добавить категорию',
    'Update Category' => 'Редактировать категорию',
    'Name' => 'Наименование',
    'Update' => 'Редактировать',
    'Create' => 'Добавить',
];